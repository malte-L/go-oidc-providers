-- name: GetOICDProvider :one
SELECT * FROM oidc_provider
WHERE id = $1 LIMIT 1;

-- name: ListOICDProviders :many
SELECT * FROM oidc_provider;

-- name: CreateOICDProvider :one
INSERT INTO oidc_provider (
  provider_url, client_id, client_secret, scopes
) VALUES (
  $1, $2, $3, $4
)
RETURNING *;

-- name: DeleteOICDProvider :exec
DELETE FROM oidc_provider
WHERE id = $1;

----------------------------


-- name: GetOICDUser :one
SELECT * FROM oidc_user
WHERE id = $1 LIMIT 1;

-- name: ListOICDUsers :many
SELECT * FROM oidc_user;

-- name: CreateOICDUser :one
INSERT INTO oidc_user (
  openid, scope_values, provider_id
) VALUES (
  $1, $2, $3
)
RETURNING *;

-- name: DeleteOICDUser :exec
DELETE FROM oidc_user
WHERE id = $1;
