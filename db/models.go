// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.13.0

package db

import (
	"encoding/json"
	"time"
)

type OidcProvider struct {
	ID           int64
	CreatedAt    time.Time
	ProviderUrl  string
	ClientID     string
	ClientSecret string
	Scopes       []string
}

type OidcUser struct {
	ID          int64
	CreatedAt   time.Time
	Openid      string
	ScopeValues json.RawMessage
	ProviderID  int64
}
