module oidc

go 1.18

require (
	github.com/coreos/go-oidc/v3 v3.2.0
	github.com/lib/pq v1.10.6
)

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/klauspost/compress v1.15.0 // indirect
	github.com/pquerna/cachecontrol v0.1.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.37.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9 // indirect
)

require (
	github.com/coreos/go-oidc v2.2.1+incompatible
	github.com/gofiber/fiber/v2 v2.34.0
	github.com/golang/protobuf v1.2.0 // indirect
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292 // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d // indirect
	golang.org/x/sync v0.0.0-20220601150217-0de741cfad7f
	google.golang.org/appengine v1.4.0 // indirect
	gopkg.in/square/go-jose.v2 v2.5.1 // indirect
)
