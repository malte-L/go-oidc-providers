CREATE TABLE oidc_provider (
  id   BIGSERIAL PRIMARY KEY,
  created_at timestamp NOT NULL DEFAULT NOW(),

  provider_url text      NOT NULL,
  client_id text         NOT NULL,
  client_secret text     NOT NULL,
  scopes text[] not null
);


CREATE TABLE oidc_user (
  id   BIGSERIAL PRIMARY KEY,
  created_at timestamp NOT NULL DEFAULT NOW(),

  openid text not null,
  scope_values json not null,
  provider_id  bigserial not null REFERENCES oidc_provider(id)
);


