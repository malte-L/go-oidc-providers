package main

import (
	"database/sql"
	"log"
	"oidc/db"
	"os"

	"github.com/gofiber/fiber/v2"
	_ "github.com/lib/pq"
)

var (
	queries *db.Queries
)

func init() {
	dbConn, err := sql.Open("postgres", "user=postgres password="+os.Getenv("DB_PW")+" dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	if err = dbConn.Ping(); err != nil {
		log.Fatal("could not reach database: " + err.Error())
	}

	queries = db.New(dbConn)
}


func main() {
	app := fiber.New()

	s := &OIDCDServer{Queries: queries, OIDCFlowDataCache: map[string]OIDCFlowData{}}
	app.Get("/authenticate/:"+ProviderIDParam, s.AuthenticateHandler())
	app.Post("/providers", s.CreateOIDCProviderHandler())
	app.Get("/dump", s.DumpDatabaseHandler())
	app.Get("/auth/google/callback", s.OCIDRedirectHandler())

	app.Listen(":4533")
}
