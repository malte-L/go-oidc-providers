package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

func post[Request, Response any](next func(Request, *fiber.Ctx) (Response, error)) fiber.Handler {
	return func(c *fiber.Ctx) error {
		log.Println("was here")

		if string(c.Request().Header.Method()) != http.MethodPost {
			return c.SendStatus(http.StatusMethodNotAllowed)
		}

		var req Request
		if err := json.Unmarshal(c.Body(), &req); err != nil {
			return c.SendStatus(http.StatusBadRequest)
		}

		return get(func(*fiber.Ctx) (Response, error) {
			return next(req, c)
		})(c)
	}
}

type ErrorWithCustomCode struct {
	Err  error
	Code int
}

func (err ErrorWithCustomCode) Error() string {
	return fmt.Sprintf("code %d, %s", err.Code, err.Err.Error())
}

func get[Response any](next func(*fiber.Ctx) (Response, error)) fiber.Handler {
	return func(c *fiber.Ctx) error {
		response, err := next(c)
		if err != nil {
			if err, ok := err.(ErrorWithCustomCode); ok {
				c.SendStatus(err.Code)
				return c.SendString(err.Error())
			}
			return c.SendStatus(http.StatusInternalServerError)
		}
		responseBytes, err := json.Marshal(response)
		if err != nil {
			return c.SendStatus(http.StatusInternalServerError)
		}

		c.Response().Header.Add("Content-Type", "application/json")
		c.SendStatus(http.StatusOK)
		return c.Send(responseBytes)
	}
}
