package main

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"io"
	"net/http"
	"oidc/db"
	"strconv"
	"time"

	"github.com/coreos/go-oidc"
	"github.com/gofiber/fiber/v2"
	"golang.org/x/oauth2"
)

type OIDCDServer struct {
	*db.Queries

	OIDCFlowDataCache map[string]OIDCFlowData
}

type OIDCFlowData struct {
	oauth2.Config
	*oidc.Provider
}

func (s *OIDCDServer) CreateOIDCProviderHandler() fiber.Handler {
	type Request struct {
		ProviderURL  string   `json:"provider_url"`
		ClientID     string   `json:"client_id"`
		ClientSecret string   `json:"client_secret"`
		Scopes       []string `json:"scopes"`
	}

	type Response struct {
		db.OidcProvider
	}

	return post(func(r Request, c *fiber.Ctx) (Response, error) {
		ctx, cancelDatabaseConn := context.WithTimeout(context.Background(), time.Second*2)
		defer cancelDatabaseConn()

		provider, err := s.Queries.CreateOICDProvider(ctx, db.CreateOICDProviderParams{
			ProviderUrl:  r.ProviderURL,
			ClientID:     r.ClientID,
			ClientSecret: r.ClientSecret,
			Scopes:       r.Scopes,
		})
		if err != nil {
			return Response{}, err
		}

		return Response{provider}, nil
	})
}

func (s *OIDCDServer) DumpDatabaseHandler() fiber.Handler {
	type Response struct {
		Users     []db.OidcUser     `json:"users,omitempty"`
		Providers []db.OidcProvider `json:"providers,omitempty"`
	}

	return get(func(c *fiber.Ctx) (Response, error) {
		ctx := context.Background()

		users, err := queries.ListOICDUsers(ctx)
		if err != nil {
			return Response{}, err
		}

		providers, err := queries.ListOICDProviders(ctx)
		if err != nil {
			return Response{}, err
		}

		return Response{users, providers}, nil
	})
}

const (
	ProviderIDParam = "provider_id"
	OIDCStateParam  = "oidc_state"
)

func (s *OIDCDServer) AuthenticateHandler() fiber.Handler {
	randString := func(nByte int) (string, error) {
		b := make([]byte, nByte)
		if _, err := io.ReadFull(rand.Reader, b); err != nil {
			return "", err
		}
		return base64.RawURLEncoding.EncodeToString(b), nil
	}

	return func(c *fiber.Ctx) error {
		id, err := strconv.ParseInt(c.Params(ProviderIDParam), 10, 64)
		if err != nil {
			return err
		}

		ctx, cancelCtx := context.WithTimeout(context.Background(), time.Second*2)
		defer cancelCtx()

		providerRecord, err := s.Queries.GetOICDProvider(context.Background(), id)
		if err != nil {
			return err
		}

		provider, err := oidc.NewProvider(ctx, providerRecord.ProviderUrl)
		if err != nil {
			return err
		}

		state, err := randString(16)
		if err != nil {
			return err
		}

		config := oauth2.Config{
			ClientID:     providerRecord.ClientID,
			ClientSecret: providerRecord.ClientSecret,
			Endpoint:     provider.Endpoint(),
			RedirectURL:  "https://m.aggelein.xyz/auth/google/callback",
			Scopes:       []string{oidc.ScopeOpenID, "profile", "email"},
		}

		s.OIDCFlowDataCache[state] = OIDCFlowData{config, provider}

		c.Cookie(&fiber.Cookie{
			Name:     "state",
			Value:    state,
			MaxAge:   int(time.Hour.Seconds()),
			Expires:  time.Time{},
			Secure:   c.Context().IsTLS(),
			HTTPOnly: true,
		})

		return c.Redirect(config.AuthCodeURL(state), http.StatusFound)
	}
}

func (s *OIDCDServer) OCIDRedirectHandler() fiber.Handler {
	return func(c *fiber.Ctx) error {
		stateCookie := c.Request().Header.Cookie("state")

		if c.Query("state") != string(stateCookie) {
			return c.SendStatus(http.StatusUnauthorized) // better code?
		}

		ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
		defer cancel()

		oidcData, ok := s.OIDCFlowDataCache[string(stateCookie)]
		if !ok {
			return c.SendStatus(http.StatusInternalServerError)
		}

		oauth2Token, err := oidcData.Config.Exchange(ctx, c.Query("code"))
		if err != nil {
			c.SendStatus(http.StatusInternalServerError)
			return c.SendString("Failed to exchange token: " + err.Error())
		}

		userInfo, err := oidcData.Provider.UserInfo(ctx, oauth2.StaticTokenSource(oauth2Token))
		if err != nil {
			c.SendStatus(http.StatusInternalServerError)
			return c.SendString("Failed to get userinfo: " + err.Error())
		}

		return c.JSON(userInfo)
	}
}
